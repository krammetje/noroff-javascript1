const feedback = document.getElementById("feedback");

//Work button
document.getElementById("workButton").addEventListener("click", function(){
    Work.GetPaid();
    UpdateMoney();
});

//Bank button
document.getElementById("bankButton").addEventListener("click",function(){
   Work.TransferToBank();
   UpdateMoney();
});

//Loan button
document.getElementById("loanButton").addEventListener("click", function (){
    if(!Loan.AddLoan(100)){
        feedback.innerText = "You can't loan any more money right now."
    } else{
        feedback.innerText = "You loan some money. You will pay this back by working."
    }
    UpdateMoney();

});

//Buy laptop button
document.getElementById("buyLaptopButton").addEventListener("click",function (){
    if(Bank.BuyLaptop()){
        feedback.innerText = "You have successfully purchased this product.";
        UpdateMoney();
    } else{
        feedback.innerText = "You don't have enough money to buy this product.";
   }
});

//Update the values of the money fields
function UpdateMoney(){
    document.getElementById("workMoney").innerText = Work.GetMoney();
    document.getElementById("loanedMoney").innerText = loanAmount;
    document.getElementById("bankMoney").innerText = Bank.GetMoney();
}