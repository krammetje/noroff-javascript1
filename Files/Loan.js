let loanAmount = 0;

class Loan{
    constructor(loanAmount = 0){
        this.loanAmount = loanAmount;
    }

    //returns the total money still owed.
    GetLoanAmount(){
        return loanAmount;
    }

    //Checks if money can be loaned. If you can't it returns false, otherwise it will add the money to the bank.
    static AddLoan(money){
        if(Bank.GetMoney()/2 < loanAmount+money){
            return false;
        }
        loanAmount += money;
        Bank.DepositMoney(money);
        return true;
    }

    //Reduced the money owed.
    ReduceLoan(money){
        loanAmount -= money;
    }

    //Set the loan back to 0.
    ResetLoan(){
        loanAmount = 0;
    }
}
