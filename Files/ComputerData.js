const computerElement = document.getElementById('computers');
const dataUrl = document.getElementById('dataUrl');
let dropdown = document.getElementById("dropdown");
const image = document.getElementById("computerImage");

let computers = [];
let currentComputer;

// Fetch the computers
function FetchComputers() {
    return fetch(
        dataUrl.getAttribute('url')
    ).then(function(response) {
        if (!response.ok) {
            throw new Error('Could not fetch the computers.');
        }
        return response.json();
    });
}

//Displays all computers
function DisplayComputers(computers = [], parentElement) {
    parentElement.innerHTML = '';
    for (const computer of computers) {
        // Create a post item
        DisplayComputer(computer, parentElement, false);
    }
}

//Displays a single computer
function DisplayComputer(computer, parentElement, replaceParent = true){
    if(replaceParent === true){
        parentElement.innerHTML = '';
    }
    const computerItem = document.createElement('li');

    // Add the title
    const computerTitle = document.createElement('h4');
    computerTitle.innerText = computer.title;
    // Add the description
    const computerDescription = document.createElement('p');
    computerDescription.innerText = computer.description;
    //Add the price
    const computerPrice = document.createElement('p');
    computerPrice.innerText = "€" + computer.price;

    computerItem.appendChild(computerTitle);
    computerItem.appendChild(computerDescription);
    computerItem.appendChild(computerPrice);
    // Attach to the parent.
    parentElement.appendChild(computerItem);

    //Display the image in the image field
    let img = new Image();
    img.src = "https://noroff-komputer-store-api.herokuapp.com/" + computer.image;
    console.log(computer.image);
    console.log(img.src);

    img.onload = function(){
        image.src = img.src;
    }
    //If the image url is invalid/doesn't work a placeholder is used instead.
    img.onerror = function(){
        image.src = "https://deconova.eu/wp-content/uploads/2016/02/default-placeholder.png"; //Default url
    }
}

//returns the price of the currently selected computer.
function GetCurrentComputerPrice(){
    return currentComputer.price;
}

async function init() {
    try {
        computers = await FetchComputers();
//        DisplayComputers(computers, computerElement);
        console.log(computers);
        for (let x in computers) {
            dropdown.options[dropdown.options.length] = new Option(computers[x].title,x);
        }
        dropdown.onchange = function() {
            DisplayComputer(computers[dropdown.value], computerElement);
            currentComputer = computers[dropdown.value];
        }
        DisplayComputer(computers[0], computerElement);
        currentComputer = computers[0];
    } catch (error) {
        console.log('ERROR!', error.message);
    }
}

init();
