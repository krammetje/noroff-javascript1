let bankMoney = 0;

class Bank{
    constructor() {

    }

    //Deposit money into the bank account.
    DepositMoney(add){
        bankMoney += add;
    }

    //Deposit money into the bank account.
    static DepositMoney(add){
        bankMoney += add;
    }

    //Reduce money by a certain amount. If it cannot be reduced, return false.
    static SpentMoney(spent){
        if(bankMoney >= spent){
            bankMoney -= spent;
            return true;
        }
            return false;
    }

    //Gets the current computer and checks if there's enough money to buy it. Returns false if it can't be bought.
    static BuyLaptop(){
        if(this.SpentMoney(GetCurrentComputerPrice())){
            return true;
        }
        return false;
    }

    static GetMoney(){
        return bankMoney;
    }
}