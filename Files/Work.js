let workMoney = 0;
let pay = 100;

class Work {
    constructor(pay = 100) {
        this.pay = pay;
    }

    //Add pay to the money in the work account
    static GetPaid() {
        if (loan.GetLoanAmount() > 0) {
            if (loan.GetLoanAmount() > pay * 0.1) {
                workMoney += pay * 0.9;
                loan.ReduceLoan(pay * 0.1);
            } else {
                workMoney += (pay - loan.GetLoanAmount());
                loan.ResetLoan();
            }
        } else{
            workMoney += pay;

        }
    }

    //Transfers money from the work account to the bank account.
    static TransferToBank() {
        bank.DepositMoney(workMoney);
        workMoney = 0;
    }

    //returns money
    static GetMoney(){
        return workMoney;
    }
}