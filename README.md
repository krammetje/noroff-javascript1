# Noroff Javascript1

## Bank

The total amount of money in your bank account is shown.
You can loan money up to half the money you have in the bank.

An error will be shown if you are unable to loan any more money.

## Work
You earn money by working.
After working you can transfer money to your bank account.
While you have a loan, 10% of the money earned from work is used to pay of the loan.

## Store

A dropdown is used to show the different laptops.
If you have made enough money, you can purchase a laptop from the store.

If no image is found for the specified laptop, a placeholder image will be shown.